@extends('layouts.app')

@section('content')
    <post :user="{{ auth()->user() }}" :data_type="{{ $data_type }}" q="{{ request('q') ?? '' }}"></post>
@endsection

