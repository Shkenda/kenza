@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <post-create-update :type="{{ $post->type }}" :user="{{ auth()->user() }}" :initial-category="{{ $categories }}" :initial-post="{{ $post }}" :editing="true"></post-create-update>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
