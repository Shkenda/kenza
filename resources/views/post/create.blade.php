@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <post-create-update :type="{{ request('type') }}" :editing="false" :user="{{ auth()->user() }}" :initial-category="{{ request('type') == 2 ? App\Category::all() : App\Category::where('name','!=','receta-gatimi')->get()  }}" ></post-create-update>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
