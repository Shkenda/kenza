@extends('layouts.app')

@section('content')
    <post-show :rating_user="{{ $rating_user ?? 0 }}" :post="{{ $post }}" :user="{{ auth()->user() ? auth()->user() : 0 }}"></post-show>
@endsection
@section('scripts')
<script>
    $(document).ready(function () {
        var Swipes = new Swiper('.swiper-container', {
            loop: false,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination',
            },
            slidesPerView: 1.1,
            spaceBetween: 32
        });
    });
</script>
@endsection