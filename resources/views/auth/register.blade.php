<!DOCTYPE html>
<html lang="en">
<head>
	<title>KENNZA</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="{{ asset('register_theme_fonts/jquery/jquery.min.js') }}"></script>
    <!-- register_theme_fonts JS-->
    <script src="{{ asset('register_theme_fonts/select2/select2.min.js') }}"></script>
    <script src="{{ asset('register_theme_fonts/datepicker/moment.min.js') }}"></script>
    <script src="{{ asset('register_theme_fonts/datepicker/daterangepicker.js') }}"></script>

    <!-- Main JS-->
    <script src="{{ asset('register_theme_js/global.js') }}" defer></script>
    <!-- Register Style -->
    <link rel="stylesheet" href="{{ asset('register_theme_fonts/mdi-font/css/material-design-iconic-font.min.css') }}">
    <link rel="stylesheet" href="{{ asset('register_theme_fonts/font-awesome-4.7/css/font-awesome.min.css' ) }}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('register_theme_fonts/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('register_theme_fonts/datepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('register_theme_css/registerStyle.css') }}">

</head>
<body>
<div class="page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins">
        <div class="wrapper wrapper--w780">
            <div class="card card-3">
                <div class="card-heading"></div>
                <div class="card-body">
                    <h2 class="title">Regjistrohu</h2>                    
                    <form method="POST" action="{{ auth()->check() ? route('store.users') : route('register') }}">
                        @csrf
                        
                        <div class="form-group ">
                            <div class="input-group">
                                <input class="input--style-3 {{ $errors->has('name') ? ' is-invalid' : ''}}" type="text" placeholder="Emri" name="name" 
                                    required autocomplete="off" autofocus>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                            <strong style="color:red;">{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                            </div> 
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <input class="input--style-3 {{ $errors->has('surname') ? ' is-invalid' : ''}}" type="text" placeholder="Mbiemri" name="surname" required autocomplete="off" autofocus>
                                @if ($errors->has('surname'))
                                    <span class="invalid-feedback">
                                        <strong style="color:red;">{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div> 
                        </div>

                        <div class="input-group">
                            <div class="rs-select2 js-select-simple select--no-search">
                                <select name="gender" require>
                                    <option disabled="disabled" selected="selected">Gjinia</option>
                                    <option>Mashkull</option>
                                    <option>Femer</option>
                                </select>
                                <div class="select-dropdown"></div>
                            </div>
                        </div>
                        @if(auth()->check() && auth()->user()->isSuperAdmin() && url()->current() == route('create.users'))
                        <div class="input-group">
                            <div class="rs-select2 js-select-simple select--no-search">
                                <select name="role_id" require>
                                    @foreach ($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                <div class="select-dropdown"></div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group">
                            <div class="input-group">
                                <input class="input--style-3 {{ $errors->has('email') ? ' is-invalid' : ''}}" type="text" placeholder="E-mail" name="email" required autocomplete="off"  autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong style="color:red;">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div> 
                        </div>


                        <div class="form-group">
                            <div class="input-group">
                                <input class="input--style-3 {{ $errors->has('password') ? ' is-invalid' : ''}}" type="password" placeholder="Fjalëkalim" name="password" required autocomplete="off" autofocus>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div> 
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <input class="input--style-3 {{ $errors->has('password') ? ' is-invalid' : ''}}" type="password" placeholder="Ri-fjalëkalim" name="password_confirmation" required autocomplete="off" autofocus>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong style="color:red;">{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div> 
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <input class="input--style-3 {{ $errors->has('phone') ? ' is-invalid' : ''}}" type="text" placeholder="Telefoni" name="phone" autocomplete="off" required autofocus>
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong style="color:red;">{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div> 
                        </div>
                        <div class="form-group">
                            <div class="p-t-10">
                                <button class="btn btn--pill btn--pink" type="submit">{{ __('Regjistrohu') }}</button>
                            </div>
                        </div>                        
                    </form>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>

