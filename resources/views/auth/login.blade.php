<!DOCTYPE html>
<html lang="en">
<head>
	<title>KENNZA</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet"> -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
    <style>
    </style>
</head>
<body>
	
	<div class="limiter" id="app">
		<div class="container-login100" style="background-image: url('../storage/images/woman-fashion-accessories.jpg');">
			<div class="wrap-login100">
                <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate>
                    @csrf
					
                    <span class="login100-form-logo">
						KENNZA
					</span>
                    <div class="wrap-input100 validate-input mail">
                        <input class="input100 email" id="email" type="email" 
                            placeholder="Email" name="email" value="" required autocomplete="off" autofocus>
                            <span class="focus-input100" data-placeholder="&#xf207;"></span>
                            <div class="invalid-tooltip" style="background-color:black;">
                                Please choose an valid email.
                            </div>
                    </div>
                    <div class="wrap-input100 validate-input"  data-validate="Enter password">
                        <input id="password" type="password" class="input100"
                            placeholder="Password" name="password" required autocomplete="current-password">
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                        <div class="invalid-tooltip" style="background-color:black;">
                            The password field it's required.
                        </div>
                    </div>

					<div class="contact100-form-checkbox">
                        <input class="input-checkbox100" type="checkbox" name="remember-me" id="ckb1" {{ old('remember') ? 'checked' : '' }}>
						<!-- <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me"> -->
						<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit">
                            <a href="/login"></a>
							Login
                        </button>

                        <button class="register100-form-btn" type="submit">
                            <a href="/register">Register
						</button>
                    </div>

					<div class="text-center p-t-90">
                        @if (Route::has('password.request'))
                            <a class="txt1" href="{{ route('password.request') }}">
                                Forgot Password?
                            </a>
                        @endif						
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="dropDownSelect1"></div>
</body>
</html>



<script>
    (function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            }
            form.classList.add('was-validated');
        }, false);
        });
    }, false);
    })();
</script>
