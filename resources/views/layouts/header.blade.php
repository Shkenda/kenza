  <div class="site-wrap ">
    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
        <div id="content-wrapper" class="d-flex flex-column">    
          <div id="content">    
            <nav class="navbar navbar-expand  topbar static-top float-right">
    
              <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
              </button>
    
              <form class="d-none d-sm-inline-block form-inline mw-100 navbar-search" action="{{ route('home') }}" method="GET">
                <div class="input-group">
                  <input type="text" class="form-control bg-light border-0 small" name="q" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary bg-pink" type="submit">
                      <i class="fas fa-search fa-sm"></i>
                    </button>
                  </div>
                </div>
              </form>
    
              <ul class="navbar-nav ml-auto">

                <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search action="{{ route('home') }}" method="GET"">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0" name="q" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary bg-pink" type="submit">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

    
                <li class="nav-item dropdown no-arrow d-sm-none">
                  <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto w-100 navbar-search">
                      <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                          <button class="btn btn-primary" type="button">
                            <i class="fas fa-search fa-sm"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </li>
    
    
                <div class="topbar-divider d-none d-sm-block"></div>
    
                <li class="nav-item dropdown no-arrow">
                  <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <img src="{{  Auth::user()->media->count() > 0 ? '/storage/'.Auth::user()->media->last()->id.'/'.Auth::user()->media->last()->file_name : "/storage/uploads/".  Auth::user()->images }}" style="width:32px; height:32px; position:absolute top:10px; left:10px; border-radius:50%">
                    <span class="mr-2 d-none d-lg-inline text-gray-600 small"> {{ Auth::user()->name }} </span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="/profile">
                      <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                      Profile
                    </a>
                    @if(Auth::user()->isSuperAdmin())
                      <a class="dropdown-item" href="/users">
                        <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                        Users
                      </a>                        
                    @endif
                    <div class="dropdown-divider"></div>
                    @guest
                      <a href="/login" class="dropdown-item" data-toggle="modal" data-target="#logoutModal">Logohu</a>
                      <a href="/register" class="dropdown-item" data-toggle="modal" data-target="#logoutModal">Regjistrohu</a></li>
                    @endguest
                    @auth
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endauth
                  </div>
                </li>    
              </ul>    
            </nav>    
          </div>    
      </div>
      <!-- End of Page Wrapper -->

      <!-- Logout Modal-->
      <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
              <a class="btn btn-primary" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
              {{-- <a class="btn btn-primary" href="login.html">Logout</a> --}}
            </div>
          </div>
        </div>
      </div>

    <header class="site-navbar js-sticky-header site-navbar-target" role="banner">
      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-sm-2">
            <h1 class="mb-0 site-logo"><a href="/" class="text-grey mb-0">KENNZA</a></h1>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                <li><a href="/" class="nav-link">Ballina</a></li>
                <li><a href="/online-shop" class="nav-link">Online Shop</a></li>
                <li disabled readonly><a disabled readonly href="#special-section" class="nav-link"><strike>Chat</strike></a></li>
                <li><a href="/receta-gatimi" class="nav-link">Receta Gatimi</a></li>
              </ul>
            </nav>
          </div>


          <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black float-right"><span class="icon-menu h3"></span></a></div>

         </div>
      </div>
      
    </header>

  
    

  </div> <!-- .site-wrap -->
