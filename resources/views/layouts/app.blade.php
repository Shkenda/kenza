<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'KENNZA') }}</title>

    <!-- Scripts -->
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('theme_fonts/icomoon/style.css') }}">
    
    <script src="{{ asset('fonts/main-font.js') }}" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <link rel="stylesheet" href="{{ asset('theme_css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('theme_css/jquery.fancybox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme_css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('theme_css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('theme_css/sb-admin-2.min.css') }}" >



    @yield('styles')

</head>
<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
    @include('layouts.header')
    <v-app id="app" class="mt-5 pt-5">
        {{-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
            @auth
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'KENNZA') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
                @endauth
            </div>
        </nav>  --}}
    <div>
        <main class="py-4">
            @yield('content')
        </main>
    </div>

    @include('layouts.footer')

    </v-app>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('theme_js/jquery-migrate-3.0.1.min.js') }}"></script>
    <script src="{{ asset('theme_js/jquery-ui.js') }}"></script>
    <script src="{{ asset('theme_js/popper.min.js') }}"></script>
    <script src="{{ asset('theme_js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('theme_js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('theme_js/jquery.stellar.min.js') }}"></script>
    <script src="{{ asset('theme_js/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('theme_js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('theme_js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('theme_js/aos.js') }}"></script>
    <script src="{{ asset('theme_js/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('theme_js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('theme_js/main.js') }}"></script>
    <script src="{{ asset('theme_js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('theme_js/bootstrap.bundle.min.js') }}"></script>

    @yield('scripts')
</body>
</html>
