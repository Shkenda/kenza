@extends('layouts.app')

@section('styles')
    <style>      
    </style>
@endsection

@section('content')	
<v-container>
    <v-flex xs12>
        <template>
            <v-row align="center" style="margin-bottom: 200px">
                <v-flex xs12 sm12 lg12 mt-5 mb-5 >
                    <h1>{{auth()->user()->name}}'s PROFILE</h1>
                    <v-form method="post" action="/profile/{{ auth()->user()->id}}" enctype="multipart/form-data" class="form-inline">
                        @csrf
                        <v-flex mt-4>
                            <v-row>
                                <v-flex xs6 sm12 lg6>
                                    <v-card-text>
                                        <v-text-field 
                                            name="name" 
                                            label="Emri" 
                                            data-vv-name="name"
                                            value="{{ auth()->user()->name }}"
                                        ></v-text-field>
                                    </v-card-text>
                                </v-flex>
                                <v-flex xs12 sm12 lg6>
                                    <v-card-text>
                                        <v-text-field  
                                        name="surname" 
                                        label="Mbiemri" 
                                        value="{{ auth()->user()->surname}}" 
                                    ></v-text-field>
                                    </v-card-text>
                                </v-flex>                               
                            </v-row>
                            <v-row>
                                <v-flex xs12 sm12 lg6>
                                    <v-card-text>
                                        <v-text-field 
                                            name="email" 
                                            label="Email"
                                            value="{{ auth()->user()->email}}" 
                                        ></v-text-field>                                       
                                    </v-card-text>
                                </v-flex>
                                <v-flex xs12 sm12 lg6>
                                    <v-card-text>
                                        <v-text-field 
                                            type="password"
                                            name="password" 
                                            label="Password"
                                        ></v-text-field>
                                    </v-card-text>
                                </v-flex>
                            </v-row>
                            <v-row>
                                <v-flex xs12 sm12 lg6>
                                    <v-card-text>
                                        <v-text-field 
                                            name="phone" 
                                            label="Telefoni" 
                                            value="{{ auth()->user()->phone}}"  
                                        ></v-text-field>
                                    </v-card-text>
                                </v-flex>
                                <v-flex xs12 sm12 lg6>
                                    <img src="{{ auth()->user()->media->count() > 0 ? '/storage/'.auth()->user()->media->last()->id.'/'.auth()->user()->media->last()->file_name : "/storage/uploads/".  auth()->user()->images }}" style="width:100px; height:100px; float:left; border-radius:50%; margin-right:25px;">
                                    <v-file-input
                                        name="images"
                                        accept="image/png, image/jpeg, image/bmp"
                                        label="File input">
                                    </v-file-input>
                                </v-flex>
                            </v-row>
                                <v-btn  class="float-right" type="submit" >Ruaj</v-btn>
                            </v-flex>
                     </v-form>
                </v-flex>
            </v-row>
        </template>
    </v-flex>
</v-container>
 {{-- <div class="container px-0">
    <h1>{{$user->name}}'s PROFILE</h1> 
     <div class="row form-group">
        <h1>{{$user->name}}'s PROFILE</h1>
        <form method="post" action="/profile/{{ $user->id}}" enctype="multipart/form-data" class="form-inline">
            @csrf
                @if($errors)
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                @endif
                <div class="col-md-6">           
                    <div class="form-group">
                        <label class="col-md-2 col-form-label">EMRI:</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 col-form-label">MBIEMRI:</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="surname" value="{{ $user->surname }}">
                        </div>
                    </div>
                </div>  
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-2 col-form-label">EMAIL:</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="email" value="{{ $user->email }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-form-label">PASSWORD:</label>
                        <div class="col-md-10">
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="col-md-2 col-form-label">PHONE:</label>
                        <div class="col-md-10">
                            <input type="number" class="form-control" name="phone" value="{{ $user->phone }}">
                        </div>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="col-md-2 form-check-label">Gender:</label>
                        <div class="col">
                            <input type="radio" class="form-check-input" name="gender">Femer 
                            <input type="radio" class="form-check-input" name="gender">Mashkull
                        </div>
                    </div>
                </div>            
                
                <div class="col-md-4">
                    <div class="form-group">
                    <img src="{{ $user->media->count() > 0 ? '/storage/'.$user->media->last()->id.'/'.$user->media->last()->file_name : "/storage/uploads/".  $user->images }}" style="width:100px; height:100px; float:left; border-radius:50%; margin-right:25px;">
                        <input type="file" name="images">
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="submit" class="pull-right btn btn-md btn-primary"> 
                </div>
            </form>
        </div>
    </div> 
    
</div>  --}}
@stop

@section('scripts')
<script>
    $(document).ready(function () {
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            document.getElementById("v-pills-companydata-tab")
                                .click();
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    });
</script>
@endsection