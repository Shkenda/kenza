<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'PostController@index')->name('home');
Route::get('post/{post}/edit', 'PostController@edit')->name('post-edit');
Route::get('post/create', 'PostController@create')->name('post-create');
Route::put('post/{post}/edit', 'PostController@update')->name('post-update');
Route::get('post/{post}', 'PostController@show')->name('post-show');
Route::post('api/posts/{post}/delete', 'PostController@destroy')->name('post-delete');
Route::post('api/posts/create', 'PostController@store')->name('post-create-form');
Route::post('api/posts/{post}/delete/image', 'PostController@detachImage');
Route::post('/api/posts/{post}/rating/{user}', 'PostController@rating');


Route::get('online-shop', 'OnlineShopController@index')->name('index.online-shop');
Route::get('receta-gatimi', 'RecetaGatimiController@index')->name('index.receta-gatimi');

Route::get('register-users', 'Auth\RegisterController@createUsers')->name('create.users');
Route::post('register-users', 'Auth\RegisterController@storeUsers')->name('store.users');

Route::get('/searching', 'SearchController@index')->name('searching');

Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

Route::get('profile/', 'Auth\UsersController@edit');

Route::post('profile/{user}', 'Auth\UsersController@update');

Route::get('/users', 'Auth\UsersController@index');

Route::post('delete/{user}', 'Auth\UsersController@destroy');
