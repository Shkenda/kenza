<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Post extends Model implements HasMedia
{
    use HasMediaTrait;

    CONST ONLINE_SHOP = 1;
    CONST RECETA_GATIMI = 2;
    CONST ALLPOST = 3;

    protected $guarded = [];

    protected $casts = ['data' => 'array'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function rating(){
        return $this->belongsToMany(User::class,'post_rating','post_id','user_id')->withPivot('post_id','user_id','rate');
    }
}
