<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;


class UsersController extends Controller
{    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->role_id != 1)
        {
            abort(403);
        }
        $users = User::latest()->get();
        return view('users.index', compact('users'));
    }

    public function edit(User $user)
    {
        return view('profile.index', array('user' => Auth::user()));
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'surname' => 'required',
            'images' => 'nullable',
            'phone' => 'required',
            'password' => 'nullable',
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
            
        ]);        
        if($request->filled('password')){
            $user->update(['password' => Hash::make($request->password)]);
        }
        $user->update([
            'name' => $request->name,
            'surname' => $request->surname,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);
        if($request->ajax()){
            return response()->json(['user' => $user],200);
        }
        if($request->hasFile('images'))
        {
            $images = $request->file('images');
            $user->addMedia($images)->toMediaCollection('images');
        }

        return redirect()->back();
    }

    public  function destroy(User $user)
    {
        $user->delete();

        return response()->json([], 200);
    }
}
