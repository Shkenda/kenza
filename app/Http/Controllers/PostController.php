<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use App\Category;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->filled('q')){
            return redirect('/searching?q='.request('q'));
        }
        if(request()->ajax()){

            return  response()->json(Post::with('media','user','category','rating')->inRandomOrder()->paginate(9));
        }
        return view('home',['data_type'=> 3]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(request('type') == null){
            return redirect()->back();
        }
        return view('post.create',[
            'categories' => Category::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.   
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'user_id' => 'required',
            'category_id' => 'nullable',
            'type' => 'required',
            'price'=> 'nullable',
            'number'=> 'nullable'
        ]);

        $post = Post::create($request->except('images','data.*','price','number'));
        $data = collect();
        if(request()->filled('price')){
            $data->put('price', request('price'));
        }
        if(request()->filled('number')){
            $data->put('number', request('number'));
        }
        if($data->count() > 0){
            $post->data = $data;
            $post->save();
        }
        if(request()->hasFile('images')){
            foreach(request('images') as $file){
                $post->addMedia($file)->toMediaCollection('images');
            }
        }
        return response()->json('You created a post successfully.',200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('post.show',[
            'post' => $post->load('media','user.media','user','category'),
            'rating_user' => optional(optional($post->rating()->where('user_id',auth()->user()->id)->first())->pivot)->rate,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('post.edit',[
            'post' => $post->load('media','user.media','category'),
            'categories' => Category::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'price' => 'nullable',
            'number' => 'nullable',
        ]);

        $post->update($request->except('images','_method','price','number'));
        $data = collect();
        if(request()->filled('price')){
            $data->put('price', request('price'));
        }
        if(request()->filled('number')){
            $data->put('number', request('number'));
        }
        if($data->count() > 0){
            $post->data = $data;
            $post->save();
        }
        if(request()->hasFile('images')){
            foreach(request('images') as $file){
                $post->addMedia($file)->toMediaCollection('images');
            }
        }
        return response()->json('You updated this post successfully.',200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        foreach($post->media as $media){
            $post->deleteMedia($media->id);
        }
        $post->delete();
        return response()->json('Successfully deleted.',200);
    }

    public function detachImage(Post $post){
        $post->deleteMedia(request('params.media'));
        return response()->json(['success' => true]);
    }

    public function rating(Post $post,User $user){
        optional(optional($post->rating()->where('post_rating.user_id',auth()->user()->id)->first())->pivot)->delete();
        $user->rating()->attach($post,['rate' => request('rate')]);
        return response()->json(['success' => true]);
    }
}
