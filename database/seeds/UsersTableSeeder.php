<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'name' => 'Admin',
            'surname' => 'Admin',
            'gender' => 'Mashkull',
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'role_id' => 1, // Super-Admin
            'password' => bcrypt('admin'), // password
            'phone' => '1234',
            'address' => '12 Ferizaj'
        ];
        User::create($user);
    }
}
