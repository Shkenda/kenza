<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use App\User;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'type' => numberBetween($min = 1, $max = 3),
        'category_id' => 1, //for now
        'user_id' => factory(User::class)->create()->id,
        'data' => array('mass' => $faker->numberBetween($min = 1, $max = 100))
    ];
});
